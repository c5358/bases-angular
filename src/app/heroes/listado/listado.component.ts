import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html'
})
export class ListadoComponent {

  heroes: string[] = ['IronMan','Spiderman','Hulk','Thor'];
  heroeBorrado: string = '';

  borrarHeroe(){

      //Opcion 1
      this.heroeBorrado =  this.heroes.shift() || '';
      //Opcion 2
      //this.heroeBorrado =  this.heroes[0];



  }

}
